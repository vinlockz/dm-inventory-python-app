from tkinter import *
from tkinter import ttk
from tkinter import filedialog
import datetime
import csv


def getfile(var):
    if var == "ours":
        global inventory, ours, invname
        ours = filedialog.askopenfilename(filetypes=[("CSV files", "*.csv")], title="OUR CSV")
        invname.set(ours)
        inventory = csv.reader(open(ours, "rt"))
    elif var == "wh":
        global warehouse, wh, whname
        wh = filedialog.askopenfilename(filetypes=[("CSV files", "*.csv")], title="MAURI CSV")
        whname.set(wh)
        warehouse = csv.reader(open(wh, "rt"))


def process():
    global warehouse, inventory
    path = filedialog.askdirectory()
    today = datetime.date.today()
    record = today.strftime('%m-%d-%Y')
    new = csv.writer(open(path+'/'+'MAURI_' + record + '_IMPORT.csv', 'w'), delimiter=',', lineterminator='\n')

    next(warehouse, None)
    next(inventory, None)

    whdict = {}

    def blank(var):
        if var == "":
            return "0"
        else:
            return str(var)

    for row in warehouse:
        style = row[0]
        color = row[1]
        size5 = row[2]
        size55 = row[3]
        size6 = row[4]
        size65 = row[5]
        size7 = row[6]
        size75 = row[7]
        size8 = row[8]
        size85 = row[9]
        size9 = row[10]
        size95 = row[11]
        size10 = row[12]
        size105 = row[13]
        size11 = row[14]
        size115 = row[15]
        size12 = row[16]
        size125 = row[17]
        size13 = row[18]
        size135 = row[19]
        size14 = row[20]
        size145 = row[21]
        size15 = row[22]
        whdict[style] = dict()
        # whdict[style]["5"] = blank(size5)
        # whdict[style]["5.5"] = blank(size55)
        whdict[style]["5"] = blank(size6)
        whdict[style]["5.5"] = blank(size65)
        whdict[style]["6"] = blank(size7)
        whdict[style]["6.5"] = blank(size75)
        whdict[style]["7"] = blank(size8)
        whdict[style]["7.5"] = blank(size85)
        whdict[style]["8"] = blank(size9)
        whdict[style]["8.5"] = blank(size95)
        whdict[style]["9"] = blank(size10)
        whdict[style]["9.5"] = blank(size105)
        whdict[style]["10"] = blank(size11)
        whdict[style]["10.5"] = blank(size115)
        whdict[style]["11"] = blank(size12)
        whdict[style]["11.5"] = blank(size125)
        whdict[style]["12"] = blank(size13)
        whdict[style]["12.5"] = blank(size135)
        whdict[style]["13"] = blank(size14)
        whdict[style]["13.5"] = blank(size145)
        whdict[style]["14"] = blank(size15)

    for rpro in inventory:
        alu = rpro[0]
        ourid = rpro[1]
        prodid = rpro[2]
        ourcolor = rpro[3]
        size = rpro[4]
        if prodid in whdict.keys():
            new.writerow([alu, whdict[prodid][size], ""])
            print(alu + " - " + prodid + " - " + ourcolor + " - " + size + " - " + whdict[prodid][size])

    print("Done.")

    root.quit()

root = Tk()
root.title("Mauri Inventory")
root.resizable(0,0)

mainframe = ttk.Frame(root,padding="3 3 20 3")
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
mainframe.columnconfigure(0, weight=1)
mainframe.rowconfigure(0, weight=1)

invname = StringVar()
whname = StringVar()

invname.set("Select the CSV that was Exported from RetailPro.")
whname.set("Select the CSV that Mauri sent.")

ttk.Button(mainframe, text="Select Mauri CSV", command=lambda: getfile("wh")).grid(column=1, row=1, sticky=W)
ttk.Label(mainframe, textvariable=whname).grid(column=1, row=2, sticky=W)
ttk.Button(mainframe, text="Select Our CSV", command=lambda: getfile("ours")).grid(column=1, row=3, sticky=W)
ttk.Label(mainframe, textvariable=invname).grid(column=1, row=4, sticky=W)
ttk.Button(mainframe, text="Process", command=process).grid(column=1, row=5, sticky=W)
ttk.Label(mainframe, text="Copyright 2015 - Version 1.0 - Dak Washbrook").grid(column=1, row=6, sticky=S+E)

for child in mainframe.winfo_children(): child.grid_configure(padx=10, pady=10)

root.bind('<Return>', process)

root.mainloop()
