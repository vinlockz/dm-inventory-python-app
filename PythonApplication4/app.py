﻿from tkinter import ttk
from tkinter import filedialog
from tkinter import messagebox
from tkinter import *
import threading
import os
import csv
import datetime
import shutil
import pandas as pd
import subprocess
import filecmp

# Titles
PROGRAM_TITLE = "Dellamoda Inventory Converter"
SELECT_UPDATE_TITLE = "Select the CSV file to Update Brand."
SELECT_INV_FILE = "Select the CSV or Excel File to Import."

# Option Menu Default
SELECT_BRAND_DEFAULT = "Select a Brand"

# Buttons
SELECT_FILE_BUTTON = "Select File"
SELECT_UPDATE_BUTTON = "Select Update"
UPDATE_BUTTON = "Update"

# Statuses
STATUSES = { 
    "SELECTFILE": "Select a File!", 
    "GO": "Click GO!",
    "CREATING": "Creating RPro %s Import CSV",
    "CONVERTING": "Converting from Excel File to usable CSV",
    "CONVERT_GO": "Excel File Conversion Successful. Click GO!"
    }

# Headers for Our Inventory
INV_HEADERS = [
    "ALU", 
    "Desc1", 
    "Desc2", 
    "Attr", 
    "Size"
    ]

# Accepted Brands
BRANDS = [
    "Select a Brand", 
    "Belvedere", 
    "Mauri",
    "Fennix"
    ]

# Update FileTypes Accepted
UPDATE_FILETYPES = [("CSV Files", "*.csv")]

# Inventory Receive FileTypes Accepted
ACCEPTED_FILETYPES = [
    ("CSV or Excel Files", "*.csv;*.xls;*.xlsx"), 
    ("CSV Files", "*.csv"), 
    ("Excel Files", "*.xls;*.xlsx")
    ]

# Inventory CSVs Local Folder
INV_CSV_FOLDER = "Inventory"

# Errors
DEFAULT_ERROR = "An error has occured"
INVALID_BRAND = "You have not selected a valid brand."
NO_CSV_ERROR = "You have not selected a file."
INCOMPLETE_UPDATE = "UPDATE NOT COMPLETED"
CSV_NOT_EXISTS = "The CSV that you have selected does not exist."
WRONG_CSV_BRAND = "The CSV you selected is not for the brand you have selected."
INVALID_FILE = "Invalid File Selected"

# About
ABOUT_TITLE = "About"
ABOUT_MSG = "The MIT License (MIT)\n\nCopyright (c) 2016 Dak Washbrook\n\nPermission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the \"Software\"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:\n\nThe above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.\n\nTHE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."

class App(object):
    def __init__(self):
        self.root = Tk()
        self.root.title(PROGRAM_TITLE)
        self.root.resizable(0, 0)
        self.root.iconbitmap('favicon.ico')

        appstyle = ttk.Style()
        appstyle.configure("TButton", padding=6, relief="flat", background="#ccc")
        appstyle.configure("TEntry", padding=6, relief="flat", background="#ccc")

        self.mainframe = ttk.Frame(self.root, padding="10 10 10 10", relief='flat')
        self.mainframe.grid(column=0, row=0, sticky=(N, W, E, S,))

        # Select the Brand
        self.brandselected = StringVar()
        self.brandselector = ttk.OptionMenu(self.mainframe, self.brandselected, *BRANDS)
        self.brandselector.grid(column=0, row=0, padx=(5, 5), pady=(5, 5), sticky=EW, columnspan=3)
        self.brandselected.set(SELECT_BRAND_DEFAULT)

        # Select the CSV to Import
        self.csvpath = StringVar()
        self.finalfile = StringVar()
        ttk.Entry(self.mainframe, textvariable=self.csvpath, width=30).grid(column=0, row=1, padx=(5, 5), pady=(5, 5), columnspan=2, sticky=EW)
        ttk.Button(self.mainframe, text=SELECT_FILE_BUTTON, command=self.getfile).grid(column=2, row=1, padx=(5, 5), pady=(5, 5), sticky=EW)

        self.gobutton = ttk.Button(self.mainframe, text="GO!", command=self.thread, state="!disabled")
        self.gobutton.grid(column=0, row=3, padx=(5, 5), pady=(5, 5), sticky=EW, columnspan=3)
        
        self.status = StringVar()
        ttk.Label(self.mainframe, textvariable=self.status, anchor="center").grid(column=0, row=4, padx=(5, 5), pady=(5, 5), columnspan=3, sticky=EW)
        self.statusupdate()

        self.newrowcount = StringVar()
        # ttk.Label(self.mainframe, textvariable=self.newrowcount).grid(column=1, row=4, padx=(5, 5), pady=(5, 5))

        self.end = StringVar()
        # ttk.Label(self.mainframe, textvariable=self.end).grid(column=0, row=5, padx=(5, 5), pady=(5, 5))

        # Progress Bar ######################
        # Progress bar is updated/set by the progress() function.
        self.progressint = IntVar()
        self.progress = ttk.Progressbar(self.mainframe, orient="horizontal", mode="determinate", variable=self.progressint, maximum=100).grid(column=0, row=5, padx=(5, 5), pady=(5, 5), columnspan=3, sticky=EW)

        self.menubar = Menu(self.root)
        # self.menubar.add_command(label="Update", command=self.updateinventory)
        self.menubar.add_command(label="About", command=self.about)
        self.root.config(menu=self.menubar)

        # Check if Conversion Happened
        self.convert_check = BooleanVar()
        self.converted_file = StringVar()
        self.convert_check.set(False)

        self.root.withdraw()
        self.root.update_idletasks()

        x = (self.root.winfo_screenwidth() - self.root.winfo_reqwidth()) / 2
        y = (self.root.winfo_screenheight() - self.root.winfo_reqheight()) / 2
        self.root.geometry("+%d+%d" % (x, y))

        self.root.deiconify()

        # Starts the GUI.
        self.root.mainloop()


    ###########################
    # UPDATE INVENTORY WINDOW #
    ###########################

    def updateinventory(self):
        self.updatewindow = Toplevel(self.root)
        windowframe = ttk.Frame(self.updatewindow, padding="10 10 10 10", relief='flat')
        windowframe.grid(column=0, row=0, sticky=(N, W, E, S,))
        
        
        self.updateselected = StringVar()
        selectupdate = ttk.OptionMenu(windowframe, self.updateselected, *BRANDS)
        selectupdate.grid(column=0, row=0, padx=(5, 5), pady=(5, 5), sticky=EW, columnspan=2)
        self.updateselected.set(SELECT_BRAND_DEFAULT)
        self.updatepath = StringVar()
        ttk.Entry(windowframe, textvariable=self.updatepath, width=30).grid(column=0, row=1, padx=(5, 5), pady=(5, 5), sticky=EW)
        ttk.Button(windowframe, text=SELECT_UPDATE_BUTTON, command=self.update).grid(column=1, row=1, padx=(5, 5), pady=(5, 5), sticky=EW)
        ttk.Button(windowframe, text=UPDATE_BUTTON, command=self.replacefiles).grid(column=1, row=2, padx=(5, 5), pady=(5, 5))

        self.updatewindow.withdraw()
        self.updatewindow.update_idletasks()

        x = (self.updatewindow.winfo_screenwidth() - self.updatewindow.winfo_reqwidth()) / 2
        y = (self.updatewindow.winfo_screenheight() - self.updatewindow.winfo_reqheight()) / 2
        self.updatewindow.geometry("+%d+%d" % (x, y))

        self.updatewindow.deiconify()
        self.updatewindow.lift()
        self.updatewindow.mainloop()

    #################################
    # Update In House Inventory CSV #
    #################################
    def update(self):
        def checkupdate(file):
            infile = open(file, "rt")
            csvin = csv.reader(infile)
            for row in csvin:
                if row[0] == INV_HEADERS[0] and row[1] == INV_HEADERS[1] and row[2] == INV_HEADERS[2] and row[3] == INV_HEADERS[3] and row[4] == INV_HEADERS[4]:
                    infile.close()
                    return True
                else:
                    infile.close()
                    return False
                break
        dest = os.getcwd() + "/" + INV_CSV_FOLDER + "/" + self.updateselected.get().lower() + ".csv"
        folder = os.getcwd() + "/" + INV_CSV_FOLDER
        update = filedialog.askopenfilename(filetypes=UPDATE_FILETYPES, title=SELECT_UPDATE_TITLE)
        self.updatewindow.lift()
        if os.path.isfile(dest):
            while filecmp.cmp(update, dest):
                self.error("The chosen file is the same as the current internal inventory file.")
                return False
                self.updatewindow.lift()
        if self.checkfile(update):
            if checkupdate(update):
                self.updatepath.set(update)

    ####################
    # Replace the CSVs #
    ####################
    def replacefiles(self):
        if self.updatepath.get() == "":
            self.error(NO_CSV_ERROR)
            return False
        selected = self.updateselected
        if selected. get().lower() == BRANDS[0].lower():
            self.error(INVALID_BRAND)
            return
        if not os.path.exists(os.getcwd() + "/" + INV_CSV_FOLDER):
            os.makedirs(os.getcwd() + "/" + INV_CSV_FOLDER)
        dest = os.getcwd() + "/" + INV_CSV_FOLDER + "/" + self.updateselected.get().lower() + ".csv"
        if shutil.copy(self.updatepath.get(), dest):
            self.done("update", False)
            self.updatewindow.destroy() # Close window
        else:
            self.error(INCOMPLETE_UPDATE)

    def statusupdate(self, status=None):
        if status == None:
            self.status.set(STATUSES["SELECTFILE"])
        else:
            self.status.set(status)

    ################################################
    # GETS THE INPUT FILE FOR CONVERSION FROM USER #
    ################################################
    def getfile(self):
        self.convert_check.set(False)
        # This function asks the user for the Import CSV.
        file = filedialog.askopenfilename(filetypes=ACCEPTED_FILETYPES, title=SELECT_INV_FILE)
        file = file
        # Set the File Path to the user chosen image.
        if file.endswith('.xls') or file.endswith('xlsx'):
            self.threadconvert(file)
        else:
            self.csvpath.set(file)
        if self.csvpath.get() != "":
            self.statusupdate(STATUSES["GO"])

    ###############################################################################
    # GETS CURRENT INVENTORY CSV FILE FROM INVENTORY FOLDER (BASED ON BRAND NAME) #
    ###############################################################################
    def getourfile(self):
        filepath = os.getcwd() + "/" + INV_CSV_FOLDER + "/" + self.brandselected.get().lower() + ".csv"
        file = open(filepath, "rt")
        next(file, None)
        return file

    ##########################################
    # CREATES THE NEW FILENAME FOR FINAL CSV #
    ##########################################
    def newfilename(self):
        self.path = filedialog.askdirectory()
        today = datetime.date.today()
        record = today.strftime('%m-%d-%Y')
        name = self.brandselected.get().upper()
        newfile = self.path + '/' + name + "_" + record + '_IMPORT.csv'
        return newfile

    ###############################
    # CHECKS THAT THE FILE EXISTS #
    ###############################
    def checkfile(self, file):
        if file == "":
            return False
        elif not os.path.isfile(file):
            self.error(CSV_NOT_EXISTS)
            return False
        else:
            return True

    ##################################################
    # CHECKS IF ANY ROWS WERE WRITTEN TO THE NEW CSV #
    ##################################################
    def checkcomplete(self, count):
        if count == 0:
            self.error(WRONG_CSV_BRAND)
            return False
        else:
            return True

    #################################
    # MAIN THREAD TO RUN CONVERSION #
    #################################
    def thread(self):
        if self.csvpath.get() == "":
            self.error(NO_CSV_ERROR)
            return False
        elif not self.checkfile(self.csvpath.get()):
            self.error(INVALID_FILE)
            return False
        selected = self.brandselected
        if selected. get().lower() == BRANDS[0].lower():
            self.error(INVALID_BRAND)
            return False
        else:
            brand = str(selected.get().lower())
            self.statusupdate(STATUSES["CREATING"] % (selected.get(),))
            t1 = threading.Thread(target=getattr(self, brand))
            t1.daemon = True
            t1.start()

    ###############################
    # LOCATES THE END OF THE FILE #
    ###############################
    def getend(self, file):
        countfile = open(file, "rt")
        endfile = csv.reader(countfile)
        row_count = sum(1 for row in endfile)
        self.end.set(str(row_count))

    #################################################
    # ANNOUNCES DONE AND REMOVES TEMP CSV IF EXISTS #
    #################################################
    def done(self, type="convert", remove=True):
        if type == "convert":
            self.openfile.close()
            if remove:
                if self.convert_check.get() == True:
                    os.remove(self.csvpath.get())
                    self.convert_check.set(False)
            messagebox.showinfo("Completed!", "%s successfully %sed!" % (self.brandselected.get(), type))
            self.statusupdate()
            self.startfile(self.path)
        elif type == "update":
            messagebox.showinfo("Completed!", "%s successfully %sd!" % (self.brandselected.get(), type))
        self.progressint.set(0)

    ################################
    # THREADS convert_xls FUNCTION #
    ################################
    def threadconvert(self, file):
        t2 = threading.Thread(target=getattr(self, "convert_xls"), args=(file,))
        t2.daemon = True
        t2.start()

    ###############################
    # CONVERTS XLS or XLSX to CSV #
    ###############################
    def convert_xls(self, file):
        self.gobutton.state(["disabled"])
        self.status.set(STATUSES["CONVERTING"])
        self.progressint.set(10)
        if file.endswith('.xls'):
            newfile = file[:-4]
        elif file.endswith('.xlsx'):
            newfile = file[:-5]
        newfile = newfile + '.csv'
        self.progressint.set(20)
        xls = pd.ExcelFile(file)
        self.progressint.set(30)
        df = xls.parse()
        self.progressint.set(40)
        df.to_csv(newfile, index=False)
        self.progressint.set(75)
        self.convert_check.set(True)
        self.progressint.set(90)
        self.csvpath.set(newfile)
        self.progressint.set(100)
        self.statusupdate(STATUSES["CONVERT_GO"])
        self.gobutton.state(["!disabled"])
        self.progressint.set(0)

    #################
    # ABOUT PROGRAM #
    #################
    def about(self):
        messagebox.showinfo(ABOUT_TITLE, ABOUT_MSG)

    ####################
    # ERROR MESSAGEBOX #
    ####################
    def error(self, msg=DEFAULT_ERROR):
        messagebox.showerror("Error!", msg)

    #########################
    # MAURI BRAND CSV LOGIC #
    #########################
    def mauri(self):
        csvout = csv.writer(open(self.newfilename(), 'w'), delimiter=',', lineterminator='\n')
        self.getend(self.getourfile().name)
        self.openfile = open(self.csvpath.get(), "rt")
        csvin = csv.reader(self.openfile)
        next(csvin, None)

        def blank(var):
            if var == "":
                return "0"
            else:
                return str(var)

        def createdict(csvin, mdict=dict()):
            for row in csvin:
                style = row[0]
                color = row[1]
                mdict[style] = dict()
                mdict[style]['5'] = row[4]
                mdict[style]['5.5'] = row[5]
                mdict[style]['6'] = row[6]
                mdict[style]['6.5'] = row[7]
                mdict[style]['7'] = row[8]
                mdict[style]['7.5'] = row[9]
                mdict[style]['8'] = row[10]
                mdict[style]['8.5'] = row[11]
                mdict[style]['9'] = row[12]
                mdict[style]['9.5'] = row[13]
                mdict[style]['10'] = row[14]
                mdict[style]['10.5'] = row[15]
                mdict[style]['11'] = row[16]
                mdict[style]['11.5'] = row[17]
                mdict[style]['12'] = row[18]
                mdict[style]['12.5'] = row[19]
                mdict[style]['13'] = row[20]
                mdict[style]['13.5'] = row[21]
                mdict[style]['14'] = row[22]
                mdict[style]['14.5'] = row[23]
                mdict[style]['15'] = row[24]

            for key, val in mdict.items():
                for k, v in mdict[key].items():
                    mdict[key][k] = blank(mdict[key][k])

            return mdict

        inventory = csv.reader(self.getourfile())

        parentloop = 0
        matchcount = 0

        mdict = createdict(csvin)
        
        theend = int(self.end.get())

        for item in inventory:
            parentloop += 1
            update = (parentloop / theend) * 100
            self.progressint.set(update)
            itemdetails = dict()
            itemdetails['alu'] = item[0]
            itemdetails['ourid'] = item[1]
            itemdetails['productid'] = item[2]
            itemdetails['color'] = item[3]
            itemdetails['size'] = item[4]
            if itemdetails['productid'] in mdict.keys():
                alu = itemdetails['alu']
                productid = itemdetails['productid']
                size = itemdetails['size']
                csvout.writerow([alu, mdict[productid][size], ""])
                matchcount += 1
                self.newrowcount.set(str(matchcount))
        if self.checkcomplete(matchcount):
            self.done()

    #############################
    # BELVEDERE BRAND CSV LOGIC #
    #############################
    def belvedere(self):
        csvout = csv.writer(open(self.newfilename(), 'w'), delimiter=',', lineterminator='\n')
        self.getend(self.csvpath.get())
        self.openfile = open(self.csvpath.get(), "rt")
        csvin = csv.reader(self.openfile)
        next(csvin, None)

        def desc_error_check(dotstring):
            if dotstring.endswith(".."):
                dotstring = dotstring[0:-2]
            return dotstring

        def getnewvars(row):
            bdict = dict()
            bdict['style'] = row[0]

            description = row[1]
            if description.endswith("BELT"):
                return False
            bdict['description'] = desc_error_check(description)

            bdict['price'] = row[2]

            quantity = row[3]
            if quantity == "-1.00" or quantity == "-1":
                quantity = "0"
            bdict['quantity'] = int(float(quantity))

            tempdesc = bdict['description'].split(" ")
            bdict['stylecode'] = tempdesc[1]

            temp = " ".join(tempdesc[2:])
            temp = temp.split("-")

            bdict['color'] = temp[0]

            bdict['size'] = str((float(temp[-1])) - 1.0)

            return bdict

        def getourvars(row):
            dictrow = dict()
            dictrow['alu'] = row[0]
            description = row[2]
            dictrow['description'] = description.split("-")
            dictrow['size'] = row[4]

            return dictrow

        matchcount = 0
        parentloop = 0
        theend = int(self.end.get())

        for row in csvin:
            parentloop += 1
            update = (parentloop / theend) * 100
            self.progressint.set(update)
            newdict = getnewvars(row)
            if not newdict:
                continue
            openour = self.getourfile()
            our = csv.reader(openour)

            for rows in our:
                ourinv = getourvars(rows)
                desc = ourinv['description']

                for test in desc[1:]:
                    if test in newdict['color']:
                        good = True
                    else:
                        good = False
                        break
                if ourinv['description'][0] in newdict['stylecode'] and ourinv['description'][-1] in newdict['color'] and float(newdict['size']) == float(ourinv['size']) and good is True:
                    csvout.writerow([ourinv['alu'], newdict['quantity']])
                    matchcount += 1
                    self.newrowcount.set(str(matchcount))
                    openour.close()
                    break
        if self.checkcomplete(matchcount):
            self.done()

    def startfile(self, filename):
        try:
            os.startfile(filename)
        except:
            subprocess.Popen(['xdg-open', filename])

    ####################
    # FENNIX CSV LOGIC #
    ####################
    def fennix(self):
        csvout = csv.writer(open(self.newfilename(), 'w'), delimiter=',', lineterminator='\n')
        self.getend(self.getourfile().name)
        self.openfile = open(self.csvpath.get(), "rt")
        csvin = csv.reader(self.openfile)
        next(csvin, None)

        ZERO = "0"
        STOCK = "1"

        def createdict(csvin, mdict=dict()):
            for row in csvin:
                if row[0].lower() == "style" or row[0].lower() == "":
                    continue
                style = row[0]
                mdict[style] = dict()
                mdict[style]['desc'] = row[1]
                if row[2].lower() == "sold out":
                    mdict[style]['7.0'] = ZERO
                    mdict[style]['7.5'] = ZERO
                    mdict[style]['8.0'] = ZERO
                    mdict[style]['8.5'] = ZERO
                    mdict[style]['9.0'] = ZERO
                    mdict[style]['9.5'] = ZERO
                    mdict[style]['10'] = ZERO
                    mdict[style]['10.5'] = ZERO
                    mdict[style]['11.0'] = ZERO
                    mdict[style]['12'] = ZERO
                    mdict[style]['13'] = ZERO
                    mdict[style]['price'] = row[13]
                else:
                    i = 2
                    size = 7
                    while i <= 12:
                        if row[i].lower() == "":
                            mdict[style][str(size)] = ZERO
                        elif row[i].lower() == "x":
                            mdict[style][str(size)] = STOCK
                        i += 1
                        if size < 11:
                            size += 0.5
                        elif size >= 11:
                            size += 1
            return mdict

        fdict = createdict(csvin)

        inventory = csv.reader(self.getourfile())

        parentloop = 0
        matchcount = 0

        theend = int(self.end.get())

        for item in inventory:
            parentloop += 1
            update = (parentloop / theend) * 100
            self.progressint.set(update)
            itemdetails = dict()
            itemdetails['alu'] = item[0]
            itemdetails['ourid'] = item[1]
            itemdetails['productid'] = item[2]
            itemdetails['color'] = item[3]
            itemdetails['size'] = item[4]
            temp = item[2].split("-")
            testcolor = temp[1]
            styleid = temp[0]
            if styleid in fdict.keys() and testcolor.lower() in fdict[styleid]['desc']:
                alu = itemdetails['alu']
                size = itemdetails['size']
                csvout.writerow([alu, fdict[styleid][size], ""])
                matchcount += 1
                self.newrowcount.set(str(matchcount))
        if self.checkcomplete(matchcount):
            self.done()


App()