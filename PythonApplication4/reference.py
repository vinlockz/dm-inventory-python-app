# Copyright 2016 Dak Washbrook / Vinlock (1/3/2016)
# Crops a SQUARE image (equal width and height) and splits them into equal smaller squares as chosen by user.
# Made for Justin :D

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
# THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import tkinter as tk
from tkinter import *
from tkinter import ttk
from tkinter import filedialog
from tkinter import messagebox
from PIL import Image
import threading
import os
import webbrowser

# Program Name
PROGRAM_TITLE = "Image Slicer by Vinlock"

# Program Labels
SLICE_SIZE_LABEL = "Slice Size:"
COLUMNS_LABEL = "Columns:"
ROWS_LABEL = "Rows:"

# Program Buttons
SELECT_IMAGE_BUTTON = "Select Image"
SLICE_BUTTON = "SLICE!"

# Errors
SIZE_ERROR_TITLE = "Size Error"
NOT_SQUARE_ERROR = "The image you selected is not a square."
INVALID_IMAGE_ERROR = "You did not select a valid image."

# About
ABOUT_STRING = "Copyright 2016 Dak Washbrook (www.vinlock.net)\n\nVersion 1.1"

class App(object):
    def __init__(self):
        # Initialize tkinter.
        self.root = Tk()
        # Set window title.
        self.root.title(PROGRAM_TITLE)
        # Disallow window resizing.
        self.root.resizable(0,0)

        # Initialize window frame.
        self.mainframe = ttk.Frame(self.root, padding="20 20 20 20")
        # Initialize frame grid.
        self.mainframe.grid(column=0, row=0, sticky=(N, W, E, S))

        # Slice Size Label.
        self.slicesizelabel = StringVar()
        self.label1 = ttk.Label(self.mainframe, textvariable=self.slicesizelabel).grid(column=0, row=1, padx=(10, 10), sticky=E)
        self.slicesizelabel.set(SLICE_SIZE_LABEL)

        # Slice Size Text Entry Box.
        self.slicesize = IntVar()
        ttk.Entry(self.mainframe, text=self.slicesize).grid(column=1, row=1, padx=(10, 10), sticky=W)
        self.slicesize.set(256)

        # Image Path Text Entry Box.
        self.imagepath = StringVar()
        ttk.Entry(self.mainframe, text=self.imagepath).grid(column=0, row=2, padx=(20, 0), pady=(20, 0), sticky=E)

        # Select Image Button ###############
        # Runs the getfile() function to get the selected image's file path and place it into the Image Path Text Entry Box.
        ttk.Button(self.mainframe, text=SELECT_IMAGE_BUTTON, command=self.getfile).grid(column=1, row=2, padx=(20, 0), pady=(20, 0), sticky=W)

        # Slice Button ######################
        # Runs the gothread() function that threads the process() function to create the new images.
        ttk.Button(self.mainframe, text=SLICE_BUTTON, command=self.gothread).grid(column=1, row=3, padx=(20, 0), pady=(20, 0), sticky=W)

        # Progress Bar ######################
        # Progress bar is updated/set by the progress() function.
        self.progressint = IntVar()
        self.progress = ttk.Progressbar(self.mainframe, orient="horizontal", length=300, mode="determinate", variable=self.progressint, maximum=100).grid(column=0, row=4, padx=(0, 0), pady=(20, 0), columnspan=2)

        # Number of Columns
        Label(self.mainframe, text=COLUMNS_LABEL).grid(column=0, row=5, padx=(20, 0), pady=(20, 0))
        self.colprogress = IntVar()
        Label(self.mainframe, textvariable=self.colprogress).grid(column=1, row=5, padx=(20, 0), pady=(20, 0))

        # Number of Rows
        Label(self.mainframe, text=ROWS_LABEL).grid(column=0, row=6, padx=(20, 0), pady=(20, 0))
        self.rowprogress = IntVar()
        Label(self.mainframe, textvariable=self.rowprogress).grid(column=1, row=6, padx=(20, 0), pady=(20, 0))

        # Top Menu
        self.menubar = tk.Menu(self.root)
        # Adds "About" to the menu and runs the function about() to show the about text.
        self.menubar.add_command(label="About", command=self.about)
        # Shows the menu.
        self.root.config(menu=self.menubar)

        # Starts the GUI.
        self.root.mainloop()

    def getfile(self):
        ##############################################################
        # This function asks the user for the JPG image file.
        ##############################################################
        file = filedialog.askopenfilename(filetypes=[("JPG files", "*.jpg")], title="Select an Image File.")
        # Set the Image Path to the user chosen image.
        self.imagepath.set(file)

    def gothread(self):
        # This function starts the thread to run process() and runs the function.
        t1 = threading.Thread(target=self.process)
        t1.daemon = True
        t1.start()

    def process(self):
        ##############################################################
        # This function processes the chosen image and slices it into
        # the user chosen square slices.
        ##############################################################

        # Be sure the image path is not blank.
        if self.imagepath.get() == "":
            self.badimage()
            return
        # Try to open the image. If it is not an image then throw an error.
        try:
           im = Image.open(open(self.imagepath.get(), 'rb'))
        except ValueError:
            self.badimage()
            return

        # Get the filename.
        self.filename = self.imagepath.get().split("/")
        self.filename = self.filename[-1]
        self.filename = self.filename.split(".")
        self.filename = self.filename[0]
        self.filename = "".join(self.filename)

        # Get the path.
        self.path = self.imagepath.get().split("/")
        self.path = "/".join(self.path[:-1])

        # Get the width and height from the user chosen image.
        width,height = im.size

        # Be sure the image is a square.
        if width != height:
            messagebox.showinfo(SIZE_ERROR_TITLE, NOT_SQUARE_ERROR)
            return

        # Store the user chosen integer slice size into a variable.
        sliceint = self.slicesize.get()

        # Be sure that the image is perfectly divisible by the slice size.
        if (width % sliceint != 0):
            messagebox.showinfo(SIZE_ERROR_TITLE, "The image you selected is not divisible by " + str(sliceint) + "x" + str(sliceint) + " squares.")
            return

        # Find how many rows/columns there will be.
        num = (width / sliceint)

        # Number of total Images.
        totalnum = num * num

        # Initialize the columns.
        col = 0

        # Initialize the number of done images.
        done = 0

        # While loop for each column.
        while col < num:
            # Initialize the rows.
            row = 0

            # While loop for each image row in column.
            while row < num:
                # Set integers to select the location of the current slice.
                left = sliceint * col
                upper = width - (sliceint * (row + 1))
                right = left + sliceint
                lower = width - (sliceint * row)

                # Create the tuple for the box with the location integers.
                box = (left, upper, right, lower)
                # Create the new image by cropping the original image.
                new = im.crop(box)

                # Create the folder under the filename of the original image if it is not already created.
                self.imgpath(self.filename)
                # Create the folder within the new filename folder under the column number.
                colpath = self.imgpath(self.filename + "/" + str(col))

                # Create the path in which to save the image.
                self.savepath = self.path + "/" + colpath + "/" + str(row) + ".jpg"
                # Create a raw string of the savepath.
                self.savepath = r""+self.savepath

                # Save the new image in the path.
                new.save(self.savepath, "JPEG")

                # Increment done to keep track of the number of images done.
                done += 1

                # % Done progress.
                theprogress = (done / totalnum) * 100
                # Set the progress bar to the % done.
                self.progressint.set(theprogress)

                # Increment the row number.
                row += 1
                # Set the row progress variable to the incremented row value.
                self.rowprogress.set(row)

            # Increment the column number.
            col += 1
            # Set the column progress variable to the incremented row value.
            self.colprogress.set(col)
        # Show message box when done.
        messagebox.showinfo("Done!", "Done!")
        return

    def about(self):
        # Shows about message box.
        messagebox.showinfo("About", ABOUT_STRING)

    def badimage(self):
        # Error when bad image.
        messagebox.showinfo("Error", INVALID_IMAGE_ERROR)

    def imgpath(self, dir):
        # If path does not exist, create it.
        fullpath = self.path + "/" + dir
        if not os.path.exists(fullpath):
            os.makedirs(fullpath)
        return dir

# Initialize the App.
App()