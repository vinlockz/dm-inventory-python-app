from tkinter import *
from tkinter import ttk
from tkinter import filedialog
import datetime
import csv


def quit():
    root.quit()


def getfile(var):
    if var == "ours":
        global ours, invname
        ours = filedialog.askopenfilename(filetypes=[("CSV files", "*.csv")], title="OUR CSV")
        invname.set(ours)
    elif var == "wh":
        global warehouse, bv, whname
        bv = filedialog.askopenfilename(filetypes=[("CSV files", "*.csv")], title="BELVEDERE CSV")
        whname.set(bv)
        warehouse = csv.reader(open(bv, "rt"))


def process():
    global warehouse, ours
    path = filedialog.askdirectory()
    today = datetime.date.today()
    record = today.strftime('%m-%d-%Y')
    new = csv.writer(open(path+'/'+'BELVEDERE_' + record + '_IMPORT.csv', 'w'), delimiter=',', lineterminator='\n')

    next(warehouse, None)

    def desc_error_check(dotstring):
        if dotstring.endswith(".."):
            dotstring = dotstring[0:-2]
        return dotstring

    for row in warehouse:
        style = row[0]
        description = row[1]
        if description.endswith("BELT"):
            continue
        description = desc_error_check(description)
        price = row[2]
        quantity = row[3]
        if quantity == "-1.00":
            quantity = "0"
        quantity = int(float(quantity))
        desc = description.split(" ")
        styleCode = desc[1]
        temp = " ".join(desc[2:])
        temp = temp.split("-")
        color = temp[0]
        size = temp[-1]
        size = float(size)
        size = (size - 1.0)
        size = str(size)
        our = open(ours, "rt")
        ourbv = csv.reader(our)
        next(our, None)
        found = False
        for rows in ourbv:
            alu = rows[1]
            ourdesc = rows[3]
            ourdesc = ourdesc.split("-")
            oursize = rows[5]
            for test in ourdesc[1:]:
                if test in color:
                    good = True
                else:
                    good = False
                    break
            if ourdesc[0] in styleCode and ourdesc[-1] in color and float(size) == float(oursize) and good is True:
                new.writerow([alu, quantity])
                our.close()
                found = True
                break

    quit()

root = Tk()
root.title("Belvedere Inventory")
root.resizable(0,0)

mainframe = ttk.Frame(root,padding="3 3 20 3")
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
mainframe.columnconfigure(0, weight=1)
mainframe.rowconfigure(0, weight=1)

invname = StringVar()
whname = StringVar()
invname.set("Select the CSV that was exported from RetailPro.")
whname.set("Select the CSV that Belvedere sent.")

ttk.Button(mainframe, text="Select Belvedere CSV", command=lambda: getfile("wh")).grid(column=1, row=1, sticky=W)
ttk.Label(mainframe, textvariable=whname).grid(column=1, row=2, sticky=W)
ttk.Button(mainframe, text="Select Our CSV", command=lambda: getfile("ours")).grid(column=1, row=3, sticky=W)
ttk.Label(mainframe, textvariable=invname).grid(column=1, row=4, sticky=W)
ttk.Button(mainframe, text="Process", command=process).grid(column=1, row=5, sticky=W)
ttk.Label(mainframe, text="Copyright 2015 - Version 1.0 - Dak Washbrook").grid(column=1, row=6, sticky=S+E)

for child in mainframe.winfo_children(): child.grid_configure(padx=10, pady=10)

root.bind('<Return>', process)

root.mainloop()